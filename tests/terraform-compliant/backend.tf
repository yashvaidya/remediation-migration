terraform {
    backend "gcs" {
        # This bucket is in the cleardata-terraform project.
        bucket = "cleardata-terraform-state"
        prefix = "testing/automated-safeguards"
    }
}
