provider "google" {
}


data "google_service_account_access_token" "admin" {
  provider = google
  target_service_account = "terraform-actor@tc1ecdc-admin-prj.iam.gserviceaccount.com"
  scopes = ["cloud-platform"]
}

provider "google" {
  alias = "impersonated"
  access_token = data.google_service_account_access_token.admin.access_token
}
