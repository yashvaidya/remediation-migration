# Terraform to Create Compliant Resource States

There are a subset of automated safeguards that don't remediate and report for
permanent resources (like orgs and projects). The terraform configurations in
this directory are a central set of configurations that can be applied, changed,
and destroyed on the permanent resources.

*NOTE:* These configs are configured for use with Terraform v0.12.
*NOTE:* There are existing organizations and projects in these configs.
*NOTE:* These configs use an impersonated service account to operate. To run
        these configs, the GCP credentials you are using need to be added
        to the 'terraform-actor' service account in the 'tc1ecdc-admin-prj'
        project in the 'test-customer-1.eng.cleardata.com' organization with
        the role of 'roles/iam.serviceAccountTokenCreator'.
*NOTE:* This may take up to 2 hours to see the changes in micromanager,
        as some are handled via publishing from inventory.

## Usage
### Import terraform state

```bash
terraform init
terraform plan
```
If the plan command shows adding or modifying resources, then that means that the associated
safeguard evaluations should be showing up in micromanager logs as not compliant.

If the plan shows that no new resources need to be added, then the associated
safeguard evaluations should be showing up in micromanager logs as compliant.

### Change up resources
#### Create resources
```bash
terraform apply
```
After this completes, you should be able to see in the Stackdriver logs
for micromanager that the resources in these configs go from non-compliant
to compliant.

#### Change resources
##### VPC Service Controls
Commenting out the 'service-perimeter' resource in the "organizations.tf"
configuration or the 'resources' in the "status" field in the
"service-perimeter" resource in the "organizations.tf" configuration and applying
it will cause the 'CloudresourcemanagerOrganizationsRequiresServiceControlsSetup'
policy for org id 896274794582 to evaluate to false.

##### Auto Grant IAM Permission Default Service Accounts
Changing 'boolean_policy.enforced' to 'false' in the 
"auto_iam_grant_default_service_accounts_policy" resource in the "organizations.tf"
configuration and applying it will cause the
'CloudresourcemanagerProjectsRestrictDefaultServiceAccountsGrants'
policy for the project eternal-courage-285322 to evaluate to false.

##### Project Trusted Images
Adding an item to 'list_policy.allow.values', commenting out 'list_policy',
or adding the field 'all = true' and commenting out 'list_policy.allow.values'
in the "list_policy.allow" in the "trusted_image_projects_policy" resource
in the "organizations.tf" configuration and applying it will cause the
'CloudresourcemanagerProjectsRequireTrustedImagePolicy'
policy for the project eternal-courage-285322 to evaluate to false.

#### Remove resources
```bash
terraform destroy
```
After this completes, the resources should go from reporting compliant to
reporting non-compliance.
