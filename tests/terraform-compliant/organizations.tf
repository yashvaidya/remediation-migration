resource "google_access_context_manager_access_policy" "access-policy" {
  # Org is test-customer-1.eng.cleardata.com
  provider = google.impersonated
  parent = "organizations/896274794582"
  title  = "safeguard_test_policy"
}

resource "google_access_context_manager_service_perimeter" "service-perimeter" {
  provider = google.impersonated
  parent = "accessPolicies/${google_access_context_manager_access_policy.access-policy.name}"
  name   = "accessPolicies/${google_access_context_manager_access_policy.access-policy.name}/servicePerimeters/test_perimeter"
  title = "safeguard_test_perimeter"
  status {
    # Adds project  eternal-courage-285322 by project number
    resources = ["projects/632455399594"]
  }
}

resource "google_organization_policy" "auto_iam_grant_default_service_accounts_policy" {
  provider = google.impersonated
  org_id     = "896274794582"
  constraint = "iam.automaticIamGrantsForDefaultServiceAccounts"

  boolean_policy {
    enforced = true
  }
}

resource "google_organization_policy" "trusted_image_projects_policy" {
  provider = google.impersonated
  org_id     = "896274794582"
  constraint = "compute.trustedImageProjects"

  list_policy {
    allow {
      values = ["projects/cleardata-images",
                "projects/cos-cloud",
                "projects/dataflow-service-producer-prod",
                "projects/cleardata-comply-images",
                "projects/cleardata-internal-images"]
    }
  }
}
