resource "google_sql_database_instance" "sqlinstance" {
  name = "cleardata-test-${random_id.test_id.hex}"
  database_version = "POSTGRES_9_6"

  settings {
    tier = "db-f1-micro"

    ip_configuration {
      ipv4_enabled = true

      require_ssl = false

      authorized_networks {
        name = "bad"
        value = "0.0.0.0/0"
      }
      authorized_networks {
        name = "good"
        value = "203.0.13.16/28"
      }
    }

    backup_configuration {
      enabled = false
    }
  }
}

