data "google_container_engine_versions" "guc1" {
  location = "us-central1"
}

resource "google_container_cluster" "cluster" {
  name = "cleardata-test-${random_id.test_id.hex}"
  location             = "us-east1"
  min_master_version = data.google_container_engine_versions.guc1.default_cluster_version

  # GCP will accept the selflink, but then changes it to the relative
  # resource name which terraform wants to change back.  So this
  # format generates the relative resource names.

  network = format(
    "projects/%s/global/networks/%s",
    google_compute_network.network.project,
    google_compute_network.network.name,
  )
  subnetwork = format(
    "projects/%s/regions/%s/subnetworks/%s",
    google_compute_subnetwork.subnet.project,
    google_compute_subnetwork.subnet.region,
    google_compute_subnetwork.subnet.name,
  )

  remove_default_node_pool = true
  initial_node_count = 0

  logging_service    = "none"
  monitoring_service = "none"

  enable_legacy_abac = true

  addons_config {
    kubernetes_dashboard {
      disabled = false
    }
  }

  node_config {
    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]
  }

  master_auth {
    username = ""
    password = ""
    client_certificate_config {
      issue_client_certificate = false
    }
  }

  network_policy {
    enabled  = true
    provider = "CALICO"
  }

}

resource "google_container_node_pool" "core-2019061301" {
  name = "cleardata-test-${random_id.test_id.hex}"
  location             = "us-east1"
  cluster            = google_container_cluster.cluster.name
  initial_node_count = 1

  node_config {
    machine_type    = "n1-standard-1"
    disk_size_gb    = 100
    image_type      = "UBUNTU"
  }

  management {
    auto_repair  = false
    auto_upgrade = false
  }
}

