data "google_iam_policy" "bad_policy" {
  binding {
    role = "roles/viewer"
    members = [
      "allUsers",
      "allAuthenticatedUsers",
      "domain:cleardata.com",
    ]
  }

  binding {
    role = "roles/editor"
    members = [
      "group:gcp-org-admins@cleardata.com",
    ]
  }
}

resource "google_pubsub_topic" "topic" {
  name = "cleardata-test-${random_id.test_id.hex}"
}

resource "google_pubsub_topic_iam_policy" "policy" {
  topic = "projects/${google_pubsub_topic.topic.project}/topics/${google_pubsub_topic.topic.name}"
  policy_data = data.google_iam_policy.bad_policy.policy_data
}

resource "google_pubsub_subscription" "sub" {
  name = "cleardata-test-${random_id.test_id.hex}"
  topic = google_pubsub_topic.topic.name

  ack_deadline_seconds = 20
}

resource "google_pubsub_subscription_iam_policy" "policy" {
  subscription = google_pubsub_subscription.sub.name
  policy_data = data.google_iam_policy.bad_policy.policy_data
}

