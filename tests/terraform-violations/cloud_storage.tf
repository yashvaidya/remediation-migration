resource "google_storage_bucket" "bucket" {
  name = "cleardata-test-${random_id.test_id.hex}"
  location = "US"

  versioning {
    enabled = false
  }

  force_destroy = true
  bucket_policy_only = true
}

resource "google_storage_bucket_iam_member" "allusers" {
  bucket = google_storage_bucket.bucket.name
  role = "roles/storage.objectViewer"
  member = "allUsers"
}

