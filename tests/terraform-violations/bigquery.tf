resource "google_bigquery_dataset" "default" {
  dataset_id = "cleardata_test_${random_id.test_id.hex}"
  friendly_name = "test"
  description = "test"
  location = "US"
  default_table_expiration_ms = 3600000

  # Note: terraform does not allow adding `allUsers`. Bigquery doesnt support it, but the apis let you add the policy.
  # We look for it, and remove it, but cant test it with this
  access {
    role = "READER"
    special_group = "allAuthenticatedUsers"
  }
  access {
    role = "OWNER"
    domain = "cleardata.com"
  }
}

