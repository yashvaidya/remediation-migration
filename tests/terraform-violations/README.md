# Terraform to create violations

This terraform config can be used to create resources that violate policy. The tfstate should not be included in version control, as it is intended to be used locally. It generates a unique ID that is included in the name of each resource it creates. When you finish testing, please `terraform destroy` to clean up the testing resources you created.

NOTE: This terraform config is intended for use with terraform v0.12

## Usage:

The following should indicate that it is going to create all of the resources used for testing:

```sh
terraform init
terraform apply
```

After this completes, you should be able to see logs in Stackdriver showing the various policy violations that were remediated. Also `terraform plan` should indicate that there are a number of changes needed to your resources for them to match the terraform config.

```sh
terraform destroy
```
