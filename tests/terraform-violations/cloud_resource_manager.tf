resource "google_project_iam_audit_config" "auditcfg" {
  service = "storage.googleapis.com"
  audit_log_config {
    log_type = "DATA_READ"
    exempted_members = [
      "user:paddy@hashicorp.com",
      "user:paddy@carvers.co",
    ]
  }
}

