import pytest
from unittest.mock import patch
from google.oauth2.credentials import Credentials
from rpe.resources.gcp import GcpProject
from google.oauth2.credentials import Credentials

import importlib

from python_policy import CloudresourcemanagerProjectsRequireServiceAccountRoleSeparation,CloudresourcemanagerProjectsRestrictOverPrivilegedIdentity

@pytest.mark.parametrize("bindings, expected, description",  [ (
    [
        {
            'role': 'roles/iam.serviceAccountAdmin',
            'members': ['user:test-user1@test.com']
        },
        {
            'role': 'roles/iam.serviceAccountUser',
            'members': ['user:test-user2@test.com']
        }
    ], True, "test1"
    ), (
    [
        {
            'role': 'roles/iam.serviceAccountAdmin',
            'members': ['user:test-user1@test.com', 'user:test-user2@test.com']
        },
        {
            'role': 'roles/iam.serviceAccountUser',
            'members': ['user:test-user3@test.com']
        },
        {
            'role': 'roles/iam.serviceAccountKeyAdmin',
            'members': ['user:test-user1@test.com', 'user:test-user2@test.com', 'user:test-user3@test.com']
        }
    ], True, "test2"),(
    [
        {
            'role': 'roles/iam.serviceAccountAdmin',
            'members': ['user:test-user1@test.com', 'user:test-user2@test.com', 'user:test-user3@test.com']
        },
        {
            'role': 'roles/iam.serviceAccountKeyAdmin',
            'members': ['user:test-user1@test.com', 'user:test-user2@test.com', 'user:test-user3@test.com']
        }
    ], True, "test3"),(
    [
        {
            'role': 'roles/iam.serviceAccountUser',
            'members': ['user:test-user1@test.com', 'user:test-user2@test.com', 'user:test-user3@test.com']
        },
        {
            'role': 'roles/iam.serviceAccountKeyAdmin',
            'members': ['user:test-user1@test.com', 'user:test-user2@test.com', 'user:test-user3@test.com']
        }
    ], True, "test4"),
    (
        [
        {
            'role': 'roles/iam.serviceAccountAdmin',
            'members': ['user:test-user1@test.com']
        },
        {
            'role': 'roles/iam.serviceAccountUser',
            'members': ['user:test-user1@test.com']
        }
    ], False, "test5"
    ), 
    (
        [
        {
            'role': 'roles/iam.serviceAccountAdmin',
            'members': ['user:test-user1@test.com', 'user:test-user2@test.com']
        },
        {
            'role': 'roles/iam.serviceAccountUser',
            'members': ['user:test-user1@test.com']
        },
        {
            'role': 'roles/iam.serviceAccountKeyAdmin',
            'members': ['user:test-user1@test.com', 'user:test-user2@test.com', 'user:test-user3@test.com']
        }
    ], False, "test6"
    ),
    (
        [
        {
            'role': 'roles/iam.serviceAccountAdmin',
            'members': ['user:test-user1@test.com', 'user:test-user2@test.com', 'user:test-user3@test.com']
        },
        {
            'role': 'roles/iam.serviceAccountUser',
            'members': ['user:test-user1@test.com', 'user:test-user2@test.com', 'user:test-user3@test.com']
        },
        {
            'role': 'roles/iam.serviceAccountKeyAdmin',
            'members': ['user:test-user1@test.com']
        }
    ], False, "test7"
    ), 
    (
        [
        {
            'role': 'roles/iam.serviceAccountAdmin',
            'members': ['user:test-user1@test.com', 'user:test-user2@test.com']
        },
        {
            'role': 'roles/iam.serviceAccountUser',
            'members': ['user:test-user2@test.com', 'user:test-user3@test.com']
        },
        {
            'role': 'roles/iam.serviceAccountKeyAdmin',
            'members': ['user:test-user1@test.com', 'user:test-user4@test.com']
        }
    ], False, "test8"
    )
])
@patch.object(CloudresourcemanagerProjectsRequireServiceAccountRoleSeparation, '_get_project_iam_bindings')
def test_identity_over_priveleged_service_account_access(mock_get_bindings, bindings, expected, description):
    mock_get_bindings.return_value = bindings
    resource = GcpProject(name="test-project", project_id="test-project-id", client_kwargs= {"credentials": Credentials(token="")})
    assert CloudresourcemanagerProjectsRequireServiceAccountRoleSeparation.compliant(resource) == expected

@pytest.mark.parametrize(
    "bindings, expected, description",
    [
        (
            [{"role": "roles/viewer", "members": ["user:test-user@test.com"]}],
            False,
            "Non Compliant: User had viewer access",
        ),
        (
            [
                {
                    "role": "roles/editor",
                    "members": [
                        "serviceAccount:test-account@cleardata-projects.iam.gserviceaccount.com",
                        "user:test-user@test.com",
                    ],
                },
                {"role": "roles/viewer", "members": ["user:test-user@test.com"]},
            ],
            False,
            "Non Compliant: Multiple identities have editor access, 1 identity has viewer access",
        ),
        (
            [
                {
                    "role": "roles/editor",
                    "members": [
                        "serviceAccount:cdsa-tst222@cleardata-credentials.iam.gserviceaccount.com"
                    ],
                },
                {
                    "role": "roles/storage.ObjectViewer",
                    "members": ["user:test-user@test.com"],
                },
            ],
            True,
            "Compliant: No identity except ClearDATA service account has "
            "owner, editor, or viewer access. ClearDATA service account excluded from check",
        ),
        (
            [
                {
                    "role": "roles/storage.ObjectUser",
                    "members": [
                        "serviceAccount:test-account@test.iam.gserviceaccount.com"
                    ],
                },
                {
                    "role": "roles/storage.ObjectViewer",
                    "members": [
                        "user:test-user@test.com",
                        "serviceAccount:test-account@test.iam.gserviceaccount.com",
                    ],
                },
            ],
            True,
            "Compliant: No identity has owner, editor, or viewer access.",
        ),
    ],
)
def test_new_compliant(bindings, expected, description):
    resource = GcpProject(name="test-project", project_id="test-project-id", client_kwargs= {"credentials": Credentials(token="")})
    with patch.object(CloudresourcemanagerProjectsRestrictOverPrivilegedIdentity, '_get_project_iam_bindings', return_value=bindings):
        with patch('python_policy.util.get_env', return_value="prod"):
            assert CloudresourcemanagerProjectsRestrictOverPrivilegedIdentity.compliant(resource) == expected
