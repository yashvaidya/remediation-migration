import random
import string

import google.auth
import jmespath
from googleapiclienthelpers.discovery import build_subresource
from googleapiclienthelpers.iam import add_binding, get_role_bindings

from .util import get_bucket_iam, get_env, get_logging_sink, get_project


class CloudresourcemanagerProjectsRequireLogBackups:
    description = "Require projects to have a log sink to backup all log data"
    applies_to = ["cloudresourcemanager.googleapis.com/Project"]
    policy_attributes = {
        "description": description,
        "evaluation": "A project is compliant if it has a log sink that "
        "routes it a google bucket and has the right permissions to write to it.",
        "remediation": "The project is remediated by setting up the "
        "log sink to a google bucket with right permissions. "
        "Any missing/non existing resource e.g. log sink, google bucket or "
        "iam permission is created automatically as part of remediation.",
    }

    BUCKET_PREFIX = "logs-{customer_id}-{project_id}"
    MANAGED_LOG_PROJECT = (
        "cleardata-logging" if (get_env() == "prod") else "cleardata-logging-nonprod"
    )
    SINK_NAME = "cleardata_log_sink"

    @classmethod
    def mk_bucket_name(cls, customer_id, project_id):
        _id = "".join(random.choice(string.ascii_lowercase) for i in range(5))
        base = cls.BUCKET_PREFIX.format(customer_id=customer_id, project_id=project_id)
        return f"{base}-{_id}"

    # Remediation requires checking each of the requirements again, so this function does both
    @classmethod
    def check(cls, resource, fix=False):
        project_id = resource.project_id
        project_data = get_project(project_id)
        customer_id = project_data["customer_id"]

        # Determine where the logs go
        # Managed customers log to a cleardata-owned project.
        # Self-service logs go to a bucket in the same project
        if project_data["self_service"]:
            log_project = project_id
            log_project_credentials = resource.client_kwargs["credentials"]
        else:
            log_project = cls.MANAGED_LOG_PROJECT
            log_project_credentials, _ = google.auth.default()

        buckets_client = build_subresource(
            "storage.buckets", "v1", credentials=log_project_credentials
        )

        bucket_name = cls.find_bucket(
            customer_id, project_id, log_project, buckets_client
        )
        if not bucket_name:
            if fix:
                bucket_name = cls.mk_bucket_name(customer_id, project_id)
                buckets_client.insert(
                    project=log_project,
                    body={
                        "name": bucket_name,
                        "iamConfiguration": {"bucketPolicyOnly": {"enabled": True}},
                        "storageClass": "NEARLINE",
                        "location": "US-CENTRAL1",
                        "lifecycle": {
                            "rule": [
                                {
                                    "action": {
                                        "type": "SetStorageClass",
                                        "storageClass": "ARCHIVE",
                                    },
                                    "condition": {"age": 45},
                                },
                            ]
                        },
                        "labels": {
                            "cleardata-purpose": "log-backups",
                            "cleardata-bucket-revision": "2",
                            "project-id": project_id,
                            "customer-id": customer_id,
                        },
                    },
                ).execute()
            else:
                return False

        # If we find a bucket, make sure the export is setup properly
        sinks_client = build_subresource(
            "logging.sinks", "v2", **resource.client_kwargs
        )
        log_sink = get_logging_sink(sinks_client, project_id, cls.SINK_NAME)

        if not log_sink:
            if fix:
                log_sink = sinks_client.create(
                    parent=f"projects/{project_id}",
                    body=cls._get_sink_definition(bucket_name),
                    uniqueWriterIdentity=True,
                ).execute()
            else:
                return False

        if not all(
            [
                log_sink["destination"] == f"storage.googleapis.com/{bucket_name}",
                log_sink.get("filter", "") == "",
                log_sink.get("disabled") is not True,
            ]
        ):
            if fix:
                log_sink = sinks_client.update(
                    sinkName=f"projects/{project_id}/sinks/{cls.SINK_NAME}",
                    body=cls._get_sink_definition(bucket_name),
                    uniqueWriterIdentity=True,
                    updateMask="destination,filter,disabled",
                ).execute()
            else:
                return False

        # Make sure the bucket policy allows the sink to write
        writer_id = log_sink.get("writerIdentity")
        bucket_iam = get_bucket_iam(buckets_client, bucket_name)

        bindings = get_role_bindings(bucket_iam, "roles/storage.objectCreator") or {}
        members = bindings.get("members", [])
        if writer_id not in members:
            if fix:
                add_binding(
                    buckets_client,
                    "roles/storage.objectCreator",
                    writer_id,
                    bucket=bucket_name,
                )

            else:
                return False

        return True

    @classmethod
    def _get_sink_definition(cls, bucket_name):
        return {
            "name": cls.SINK_NAME,
            "disabled": False,
            "filter": "",
            "destination": f"storage.googleapis.com/{bucket_name}",
        }

    @classmethod
    def find_bucket(cls, customer_id, project_id, log_project, buckets_client):
        prefix = cls.BUCKET_PREFIX.format(
            project_id=project_id, customer_id=customer_id
        )

        resp = buckets_client.list(
            project=log_project,
            prefix=prefix,
        ).execute()

        buckets = resp.get("items", [])

        matching_buckets = []

        # There may be more than 1 matching bucket if we modify our standard
        # config, we want the latest revision
        for bucket in buckets:
            bucket_name = bucket["name"]
            bucket_labels = bucket.get("labels", {})

            # Exact match indicates our initial bucket naming convention
            # After we migrate all buckets, this logic can go away
            if bucket_name == prefix:
                bucket_revision = 0
            else:
                bucket_revision = cls._get_bucket_revision(bucket_labels)

            # If the bucket has a project-id label, and its for a different
            # project, this is not a matching bucket
            # This is helpful if a project_id is a prefix to another project_id
            label_project_id = bucket_labels.get("project-id")
            if label_project_id and label_project_id != project_id:
                continue

            # Need to test against None because it can be zero
            if bucket_revision is not None:
                matching_buckets.append((bucket_revision, bucket_name))

        if matching_buckets:
            # We want the highest revision
            revision, name = list(sorted(matching_buckets))[-1]
            return name

    # We've changed the bucket configuration twice now, lets make sure we can identify that
    @classmethod
    def _get_bucket_revision(cls, labels):
        # Later revisions added a cleardata-purpose label
        if labels.get("cleardata-purpose") == "log-backups":
            # even later revisions added the revision label
            bucket_revision = int(labels.get("cleardata-bucket-revision", "1"))

            return bucket_revision

    @classmethod
    def compliant(cls, resource):
        return cls.check(resource, fix=False)

    @classmethod
    def excluded(cls, resource):
        safeguard_config = jmespath.search(
            'resource.labels."cleardata-safeguards"', resource.get(refresh=False)
        )
        return safeguard_config == "disabled"

    @classmethod
    def remediate(cls, resource):
        cls.check(resource, fix=True)
