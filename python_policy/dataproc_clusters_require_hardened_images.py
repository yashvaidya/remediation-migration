import re

import googleapiclient.errors
import jmespath
import tenacity
from googleapiclienthelpers.discovery import build_subresource

from .util import get_config_context


class DataprocClustersRequireHardenedImages:
    description = "Validate Dataproc source images are compliant"
    applies_to = ["dataproc.googleapis.com/Cluster"]

    policy_attributes = {
        "description": description,
        "evaluation": "A Dataproc cluster is compliant if worker base images are hardened by ClearDATA",
        "remediation": "A non-compliant cluster is remediated by being deleted",
        "notes": "",
    }

    @classmethod
    def compliant(cls, resource):
        # Get cluster configuration and extract masterConfig
        cluster_info = resource.get(refresh=False)
        master_config = jmespath.search("resource.config.masterConfig", cluster_info)
        cd_licenses = get_config_context("gcp.compute.licenses")

        if cls._is_image_from_approved_project(master_config):
            return True

        cluster_config_image_sources = set()

        # Get a set of source images for dataproc configs
        for worker in ["masterConfig", "workerConfig", "secondaryWorkerConfig"]:
            imageUri = jmespath.search(
                f"resource.config.{worker}.imageUri", cluster_info
            )
            if imageUri:
                cluster_config_image_sources.add(imageUri)

        # Get disk information
        disks = cls._get_disks_context(resource, cluster_info)

        # If the disk has a source in our set. Confirm it has a license from us
        for disk in disks:
            source_image = disk["sourceImage"]

            # If the disks source image is not in the list, continue
            if source_image not in cluster_config_image_sources:
                continue

            # Validate the licenses on the disk. If no CD liscense is present
            # return that its not compliant
            if not cls._validate_disk(cd_licenses, disk):
                return False

            cluster_config_image_sources.remove(source_image)

            if len(cluster_config_image_sources) == 0:
                return True
        return False

    @classmethod
    def _validate_disk(cls, cd_licenses, disk):
        for cd_license in cd_licenses:
            for disk_license in disk.get("licenses", []):
                # cd_license project/<project_id>/global/licenses/<license>
                # disk_license https://compute.google.../project/<project_id>/global/licenses/<license>
                if disk_license.endswith(cd_license):
                    return True

    @classmethod
    @tenacity.retry(
        wait=tenacity.wait_random_exponential(multiplier=15, max=60),
        stop=tenacity.stop_after_attempt(3),
    )
    def _get_disks_context(cls, resource, cluster_info):
        compute_disks_client = build_subresource(
            "compute.disks", "v1", **resource.client_kwargs
        )

        # On a get request, zone will always be present. A full URL, partial URI,
        # or short name are valid. Examples:
        # https://www.googleapis.com/compute/v1/projects/[project_id]/zones/[zone]
        # projects/[project_id]/zones/[zone]
        # us-central1-f
        zone_uri = cluster_info["resource"]["config"]["gceClusterConfig"]["zoneUri"]
        disk_zone = zone_uri.split("/")[-1]

        disks_yieled = 0
        req = compute_disks_client.list(
            project=resource.project_id,
            zone=disk_zone,
            filter=f"labels.goog-dataproc-cluster-name={resource.resource_name}",
        )
        while req is not None:
            # If this fails tenacity will catch it and retry
            resp = req.execute()

            for disk in resp.get("items", []):
                yield disk
                disks_yieled += 1

            req = compute_disks_client.list_next(req, resp)

        # If we don't get any disks; error and let tenacity retry
        if disks_yieled == 0:
            raise Exception(
                "No disks found for this cluster yet, cannot complete evaluation"
            )

    @classmethod
    def _is_image_from_approved_project(cls, master_config):
        dataproc_approved_images = get_config_context(
            "gcp.dataproc.harden_images_project"
        )

        # If the source image is from a approved project return True
        cluster_image_project_id = re.search(
            "projects/(.*?)/", master_config["imageUri"]
        ).group(1)

        if cluster_image_project_id in dataproc_approved_images:
            return True

    # This is handeled elsewhere to reduce duplication
    @classmethod
    def excluded(cls, resource):
        return False

    # Happens automatically if not compliant
    @classmethod
    def remediate(cls, resource):
        resource_data = resource.to_dict()
        cluster_info = resource.get(refresh=False)["resource"]
        dataproc_cluster_client = build_subresource(
            "dataproc.projects.regions.clusters", "v1beta2", **resource.client_kwargs
        )

        req = dataproc_cluster_client.delete(
            projectId=resource_data["project_id"],
            region=resource_data["location"],
            clusterName=resource_data["name"],
            clusterUuid=cluster_info["clusterUuid"],
            requestId="real-time-enforcer-delete",
        )
        try:
            req.execute()
        except googleapiclient.errors.HttpError as ex:
            if ex.resp.status != 404:
                raise
