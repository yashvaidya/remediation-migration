import jmespath
from googleapiclienthelpers.discovery import build_subresource


class CloudresourcemanagerProjectsRestrictDefaultServiceAccountsGrants:
    description = "Restrict the use of the default service account wide scope of roles"
    applies_to = ["cloudresourcemanager.googleapis.com/Project"]
    policy_attributes = {
        "description": description,
        "evaluation": "A project is compliant if the automatic role grants "
        "to default service accounts is disabled. This is done "
        "by enforcing the boolean policy constraint "
        "on iam.automaticIamGrantsForDefaultServiceAccounts.",
        "remediation": "N/A",
    }

    @classmethod
    def compliant(cls, resource):
        service = build_subresource(
            "cloudresourcemanager.projects", "v1", **resource.client_kwargs
        )
        org_policy_body = {
            "constraint": "constraints/iam.automaticIamGrantsForDefaultServiceAccounts"
        }
        policy = service.getEffectiveOrgPolicy(
            resource=f"projects/{resource.project_id}", body=org_policy_body
        ).execute()
        policy_is_enforced = policy.get("booleanPolicy", {}).get("enforced")
        return True if policy_is_enforced else False

    @classmethod
    def excluded(cls, resource):
        safeguard_config = jmespath.search(
            'resource.labels."cleardata-safeguards"', resource.get()
        )
        return safeguard_config == "disabled"
