import jmespath
from googleapiclient.discovery import build
from functools import lru_cache
import re

from .util import get_cleardata_service_account_regex, get_user_managed_service_account_regex

class CloudresourcemanagerProjectsRestrictOverPrivilegedIdentity:
    description = (
        "Restrict the use of an identity which has Owner, Editor, and/or Viewer role to the Project."
        "Applies to identities granted access to the only the Project Level, inherited identities and "
        "ClearDATA service account excluded from check. "
    )
    applies_to = ["cloudresourcemanager.googleapis.com/Project"]
    policy_attributes = {
        "description": description,
        "evaluation": "A project is compliant if no identity has Owner, Editor, and/or Viewer role to the Project."
        "Applies to identities granted access to the only the Project Level, inherited identities and "
        "ClearDATA service account excluded from check.  ",
        "remediation": "N/A",
    }

    @classmethod
    @lru_cache
    def _get_service_account_regex(cls):
        return (
            get_cleardata_service_account_regex(),
            get_user_managed_service_account_regex(),
        )

    @classmethod
    def _get_project_iam_bindings(cls, resource):
        service = build("cloudresourcemanager", "v1", **resource.client_kwargs)
        policy = service.projects().getIamPolicy(resource=resource.project_id).execute()
        return policy.get("bindings")

    @classmethod
    def _over_privileged_binding(cls, binding):
        bindings_list = ["roles/owner", "roles/editor", "roles/viewer"]
        cleardata_service_account_regex, user_managed_service_account_regex = (
            cls._get_service_account_regex()
        )
        if binding["role"] in bindings_list:
            for member in binding["members"]:
                if (
                    not member.startswith("serviceAccount:")
                    or (user_managed_service_account_regex.search(member) and not cleardata_service_account_regex.search(member))
                ):
                    return True
        return False

    @classmethod
    def compliant(cls, resource):
        bindings = cls._get_project_iam_bindings(resource)
        for binding in bindings:
            if cls._over_privileged_binding(binding):
                return False
        return True

    @classmethod
    def excluded(cls, resource):
        safeguard_config = jmespath.search(
            'resource.labels."cleardata-safeguards"', resource.get()
        )
        return safeguard_config == "disabled"
