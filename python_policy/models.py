class V2ComplianceState:
    PASS = "pass"
    FAIL = "fail"
    NOT_APPLICABLE = "not_applicable"
    INCOMPLETE = "incomplete"
