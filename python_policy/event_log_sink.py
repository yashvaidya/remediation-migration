from types import SimpleNamespace

import google.auth
import jmespath
from googleapiclienthelpers.discovery import build_subresource
from googleapiclienthelpers.iam import add_binding, get_role_bindings

from .util import (
    compare_logging_sinks,
    create_logging_sink,
    get_env,
    get_logging_sink,
    get_project,
    update_logging_sink,
)


class CloudresourcemanagerProjectsRequireEventsSink:
    description = "Require projects to have a log sink to backup all log data"
    applies_to = ["cloudresourcemanager.googleapis.com/Project"]
    policy_attributes = {
        "description": description,
        "evaluation": "The project is compliant if it has a log sink "
        "which routes to cleardata-project-events topic and "
        "has the necessary permissions for this operation.",
        "remediation": "The project is remediated by setting up a "
        "log sink to the cleardata-project-events topic "
        "and adding any missing permissions required for this operation.",
    }

    MANAGED_LOG_PROJECT = (
        "cleardata-logging" if (get_env() == "prod") else "cleardata-logging-nonprod"
    )

    @classmethod
    def _required_data(cls, resource):
        d = SimpleNamespace()

        d.project_data = get_project(resource.project_id)
        d.customer_id = d.project_data["customer_id"]
        d.sink_name = "cleardata_events_sink"

        d.topic_id = (
            f"projects/{cls.MANAGED_LOG_PROJECT}/topics/cleardata-project-events"
        )
        d.app_creds, _ = google.auth.default()

        d.expected_sink = {
            "name": d.sink_name,
            "destination": f"pubsub.googleapis.com/{d.topic_id}",
            "filter": 'logName:"logs/cloudaudit.googleapis.com%2Factivity" severity>INFO',
        }

        d.sinks_client = build_subresource(
            "logging.sinks", "v2", **resource.client_kwargs
        )

        d.pubsub_client = build_subresource(
            "pubsub.projects.topics", "v1", credentials=d.app_creds
        )

        d.sink = get_logging_sink(d.sinks_client, resource.project_id, d.sink_name)

        return d

    @classmethod
    def compliant(cls, resource):
        try:
            d = cls._required_data(resource)

            if not d.sink:
                return False

            if not compare_logging_sinks(d.expected_sink, d.sink):
                return False

            # Does it have permissions to write to the target bucket?
            writer_id = d.sink.get("writerIdentity")
            topic_iam = d.pubsub_client.getIamPolicy(resource=d.topic_id).execute()

            if (
                writer_id
                not in get_role_bindings(topic_iam, "roles/pubsub.publisher")["members"]
            ):
                return False

            return True
        except Exception as e:
            raise e

    @classmethod
    def excluded(cls, resource):
        safeguard_config = jmespath.search(
            'resource.labels."cleardata-safeguards"', resource.get()
        )
        return safeguard_config == "disabled"

    @classmethod
    def remediate(cls, resource):
        d = cls._required_data(resource)

        # If there's no sink, create one
        if not d.sink:
            d.sink = create_logging_sink(
                d.sinks_client, resource.project_id, d.sink_name, d.expected_sink
            )

        # If one exists and isn't valid, update it
        elif not compare_logging_sinks(d.expected_sink, d.sink):
            d.sink = update_logging_sink(
                d.sinks_client, resource.project_id, d.sink_name, d.expected_sink
            )

        writer_id = d.sink.get("writerIdentity")
        add_binding(
            d.pubsub_client, "roles/pubsub.publisher", writer_id, resource=d.topic_id
        )
