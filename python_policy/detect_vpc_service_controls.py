from googleapiclienthelpers.discovery import build_subresource


class CloudresourcemanagerOrganizationsRequiresServiceControlsSetup:
    description = "Detect whether organization has VPC Service Controls setup"
    applies_to = ["cloudresourcemanager.googleapis.com/Organization"]

    policy_attributes = {
        "description": description,
        "evaluation": "An organization's service control set up is "
        "compliant if there is at least one access policy "
        "with at least one service perimeter that contains any resources.",
        "remediation": "N/A",
    }

    @classmethod
    def compliant(cls, resource):  # organization
        """
        Check if there is at least one access policy with at least one service perimeter that contains
        any resources.
        """
        policy_service = build_subresource(
            "accesscontextmanager.accessPolicies", "v1", **resource.client_kwargs
        )
        perimeter_service = build_subresource(
            "accesscontextmanager.accessPolicies.servicePerimeters",
            "v1",
            **resource.client_kwargs,
        )
        organization = resource.to_dict()["name"]
        policy_response = policy_service.list(
            parent=f"organizations/{organization}"
        ).execute()
        access_policies = policy_response.get("accessPolicies", [])

        for policy in access_policies:
            response = perimeter_service.list(parent=policy["name"]).execute()
            for perimeter in response.get("servicePerimeters", []):
                perimeter_service.get(name=perimeter["name"]).execute()
                if perimeter["status"].get("resources") is not None:
                    return True

        return False

    @classmethod
    def excluded(cls, resource):
        return False
