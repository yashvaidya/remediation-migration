import uuid

from googleapiclienthelpers.discovery import build_subresource

from .util import randstr


class ComputeNetworksProhibitInboundDHCPResponses:
    description = "Ensure there is a firewall rule blocking inbound DHCP responses"
    applies_to = ["compute.googleapis.com/Network"]

    policy_attributes = {
        "description": description,
        "evaluation": "A Compute Network is compliant if there is a firewall rule blocking inbound DHCP packets",
        "remediation": "A non-compliant network is remediated by adding the desired firewall rule",
        "notes": "This safeguard was created due to a vulnerability "
        "disclosed here: https://github.com/irsl/gcp-dhcp-takeover-code-exec",
    }

    compute_firewall_prefix = "cleardata-block-udp-68"

    firewall_base = {
        "denied": [{"IPProtocol": "udp", "ports": ["68"]}],
        "description": "",
        "direction": "INGRESS",
        "disabled": False,
        "priority": 0,
        "logConfig": {"enable": True},
        "sourceRanges": ["0.0.0.0/0"],
    }

    @classmethod
    def _get_firewall_client(cls, resource):
        client_kwargs = resource.client_kwargs
        firewall_client = build_subresource("compute.firewalls", "v1", **client_kwargs)
        return firewall_client

    @classmethod
    def _validate_firewall(cls, rule):
        if not rule.get("name").startswith(cls.compute_firewall_prefix):
            return False

        for key in cls.firewall_base.keys():
            if cls.firewall_base.get(key) != rule.get(key):
                return False

        return True

    @classmethod
    def compliant(cls, resource):
        firewall_client = cls._get_firewall_client(resource)

        resource_name = resource.resource_name
        network_name = f"https://www.googleapis.com/compute/v1/projects/{resource.project_id}/global/networks/{resource_name}"

        req = firewall_client.list(
            project=resource.project_id,
            filter=f'network = "{network_name}"',
        )

        while req is not None:
            resp = req.execute()

            for rule in resp.get("items", []):
                if cls._validate_firewall(rule):
                    return True

            req = firewall_client.list_next(req, resp)

        return False

    @classmethod
    def excluded(cls, resource):
        return False

    @classmethod
    def remediate(cls, resource):
        firewall_client = cls._get_firewall_client(resource)

        resource_name = resource.resource_name
        network_name = f"projects/{resource.project_id}/global/networks/{resource_name}"

        request_id = uuid.uuid5(uuid.NAMESPACE_URL, network_name)

        payload = cls.firewall_base.copy()
        payload["name"] = f"{cls.compute_firewall_prefix}-{randstr()}"
        payload["network"] = network_name

        req = firewall_client.insert(
            project=resource.project_id,
            body=payload,
            requestId=request_id,
        )
        req.execute()
