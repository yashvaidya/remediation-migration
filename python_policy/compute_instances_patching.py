from datetime import datetime, timedelta, timezone

import jmespath

from . import clients
from .util import fromiso_tz


class ComputeInstancesRequireUpToDatePatching:
    description = "Require compute instances to be up to date on patches"
    applies_to = ["compute.googleapis.com/Instance"]

    policy_attributes = {
        "description": description,
        "evaluation": """
        A Compute instance is compliant if all of the following conditions are true:
        * The OS config agent has reported the package status within the threshold
        * Any available package updates have been know for less than the threshold
        """,
        "remediation": "",
        "notes": "",
    }

    @classmethod
    def compliant(cls, resource):  # noqa C901
        threshold = cls.get_patch_age_threshold()

        # If the instance isn't old, it can't have old patches. This should
        # help us in the case where a customer re-uses a name for an instance
        # that we already have in inventory

        resource_data = resource.get(refresh=False)
        create_time = jmespath.search("resource.creationTimestamp", resource_data)

        try:
            if fromiso_tz(create_time) > threshold:
                return True
        except (ValueError, TypeError):
            pass

        # Lookup the instance metadata in the inventory API
        # Note: our client defaults to raising on a bad http status code
        try:
            resp = clients.inventory.get(
                "/compute-instance-metadata/patching-summary",
                params={"full_resource_name": resource.full_resource_name()},
            )

        except Exception as e:
            raise e

        data = resp.json()

        # Since we know the instance is at least old enough, and we don't have
        # any data, we should assume they're not compliant
        if not data:
            return False

        agent_update_time = jmespath.search("os_config_agent_last_run", data)
        oldest_patch_time = jmespath.search("oldest_package_update.first_seen", data)
        num_old_patches = len(data.get("oldest_package_update", []))

        # If we don't have data
        if agent_update_time is None:
            return False

        # If the agent hasn't updated the patch data within the threshold
        if fromiso_tz(agent_update_time) < threshold:
            return False

        # If there are zero patches we're fine
        if num_old_patches == 0:
            return True

        # If there is an old patch
        if fromiso_tz(oldest_patch_time) < threshold:
            return False

        # If we haven't found any issues thus far
        return True

    @classmethod
    def get_patch_age_threshold(cls):
        # Currently hard-coded value
        age_threshold_days = 90
        cutoff_date = datetime.utcnow() - timedelta(days=age_threshold_days)
        return cutoff_date.replace(tzinfo=timezone.utc)

    # We're handle this outside of the policies now, eventually we should drop
    # it from here
    @classmethod
    def excluded(cls, resource):
        return False
