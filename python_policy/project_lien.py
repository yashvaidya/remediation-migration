import jmespath
from googleapiclienthelpers.discovery import build_subresource


class CloudresourcemanagerProjectsRequireClearDATALien:
    description = "Require projects to have a cleardata lien to prevent deletion"
    applies_to = ["cloudresourcemanager.googleapis.com/Project"]
    policy_attributes = {
        "description": description,
        "evaluation": "A project is compliant if it has a "
        "lien (resourcemanager.projects.delete) on "
        "cleardata as origin (onboarding.cleardata.com).",
        "remediation": "N/A",
    }

    @classmethod
    def compliant(cls, resource):
        liens_client = build_subresource(
            "cloudresourcemanager.liens", "v1", **resource.client_kwargs
        )

        req = liens_client.list(parent=f"projects/{resource.project_id}")
        resp = req.execute()
        while resp:
            liens = resp.get("liens")
            for lien in liens:
                if "resourcemanager.projects.delete" not in lien.get(
                    "restrictions", []
                ):
                    continue

                if lien.get("origin") == "onboarding.cleardata.com":
                    return True

            resp = liens_client.list_next(req, resp)

        return False

    @classmethod
    def excluded(cls, resource):
        safeguard_config = jmespath.search(
            'resource.labels."cleardata-safeguards"', resource.get()
        )
        return safeguard_config == "disabled"
