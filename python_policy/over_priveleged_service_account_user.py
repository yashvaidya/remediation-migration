import jmespath
from googleapiclient.discovery import build


class CloudresourcemanagerProjectsRestrictOverPrivilegedServiceAccountUser:
    description = (
        "Restrict the use of an identity which has Service Account User "
        "and/or Service Account Token Creator access at the Project Level. Applies to identities granted "
        "access to the only the Project Level, inherited identities excluded from check. "
    )
    applies_to = ["cloudresourcemanager.googleapis.com/Project"]
    policy_attributes = {
        "description": description,
        "evaluation": "A project is compliant if no identity has Service Account Token Creator "
        "and/or Service Account User access at the Project Level. Applies to identities granted "
        "access to the only the Project Level, inherited identities excluded from check.  ",
        "remediation": "N/A",
    }

    @classmethod
    def _get_project_iam_bindings(cls, resource):
        service = build("cloudresourcemanager", "v1", **resource.client_kwargs)
        policy = service.projects().getIamPolicy(resource=resource.project_id).execute()
        return policy.get("bindings")

    @classmethod
    def _over_privileged_binding(cls, binding):
        bindings_list = [
            "roles/iam.serviceAccountUser",
            "roles/iam.serviceAccountTokenCreator",
        ]
        if binding["role"] in bindings_list:
            return True

        return False

    @classmethod
    def compliant(cls, resource):
        bindings = cls._get_project_iam_bindings(resource)
        for binding in bindings:
            if cls._over_privileged_binding(binding):
                return False
        return True

    @classmethod
    def excluded(cls, resource):
        safeguard_config = jmespath.search(
            'resource.labels."cleardata-safeguards"', resource.get()
        )
        return safeguard_config == "disabled"
