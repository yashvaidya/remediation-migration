import re
from functools import lru_cache

import jmespath
from googleapiclient.discovery import build

from .util import get_env


class CloudresourcemanagerProjectsRestrictOverPrivilegedServiceAccount:
    description = (
        "Restrict the use of a non inherited user created service account"
        "which Owner, Editor or any Admin privilege to the project. ClearDATA service account excluded from check."
    )
    applies_to = ["cloudresourcemanager.googleapis.com/Project"]
    policy_attributes = {
        "description": description,
        "evaluation": "A project is compliant if no non inherited user created service account "
        "has Owner, Editor, or any Admin access to the project. ClearDATA service account excluded from check.",
        "remediation": "N/A",
    }

    @classmethod
    def _get_project_iam_bindings(cls, resource):
        service = build("cloudresourcemanager", "v1", **resource.client_kwargs)
        policy = service.projects().getIamPolicy(resource=resource.project_id).execute()
        return policy.get("bindings")

    @classmethod
    @lru_cache
    def _get_service_account_regex(cls):
        credentials_project = (
            "cleardata-credentials"
            if (get_env() == "prod")
            else "cleardata-creds-nonprod"
        )
        non_cleardata_user_managed_service_account_regex = re.compile(
            rf"serviceAccount:(?!service-)[a-zA-Z0-9_.-]+@((?!{credentials_project}\.)[a-zA-Z0-9_-]+).iam.gserviceaccount.com"
        )
        return non_cleardata_user_managed_service_account_regex

    @classmethod
    def _over_privileged_binding(cls, binding):
        bindings_list = ["roles/owner", "roles/editor"]
        service_account_regex = cls._get_service_account_regex()
        if binding["role"] in bindings_list or "admin" in binding["role"]:
            for member in binding["members"]:
                if service_account_regex.search(member):
                    return True

        return False

    @classmethod
    def compliant(cls, resource):
        bindings = cls._get_project_iam_bindings(resource)
        for binding in bindings:
            if cls._over_privileged_binding(binding):
                return False
        return True

    @classmethod
    def excluded(cls, resource):
        safeguard_config = jmespath.search(
            'resource.labels."cleardata-safeguards"', resource.get()
        )
        return safeguard_config == "disabled"
