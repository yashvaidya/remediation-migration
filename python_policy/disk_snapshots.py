import googleapiclient.errors
import jmespath
from googleapiclienthelpers.discovery import build_subresource
from rpe.policy import EvaluationResult

from .models import V2ComplianceState
from .util import gen_request_id, get_url_value_by_collection


class _DiskSnapshotBase:
    description = "Require compute disks to have snapshot schedules"

    DEFAULT_RESOURCE_POLICY_NAME = "cleardata-snapshot-schedule"

    @classmethod
    def evaluate(cls, resource):
        evaluation_attributes = {}

        # No need to setup snapshots for disks used by dataflow jobs
        if "dataflow_job_id" in resource.labels:
            evaluation_attributes["v2_compliance_state"] = (
                V2ComplianceState.NOT_APPLICABLE
            )

        ev = EvaluationResult(
            compliant=cls.compliant(resource),
            remediable=True,
            evaluation_attributes=evaluation_attributes,
        )

        return ev

    @classmethod
    def compliant(cls, resource):
        resource_policies = jmespath.search(
            "resource.resourcePolicies", resource.get(refresh=False)
        )

        # If no resource policies, we're not compliant
        if not resource_policies:
            return False

        rp_client = cls._get_rp_client(**resource.client_kwargs)

        # There's multiple types of resource policies, we need to make sure
        # theres at least 1 snapshot schedule policy
        for rp in resource_policies:
            if cls._is_snapshot_schedule(rp_client, rp):
                return True

        return False

    @classmethod
    def excluded(cls, resource):
        return False

    @classmethod
    def remediate(cls, resource):
        rp_client = cls._get_rp_client(**resource.client_kwargs)

        res_data = resource.to_dict()
        disk_name = res_data.get("name")
        location = res_data.get("location")
        region = "-".join(location.split("-")[:2])
        project_id = resource.project_id

        default_rp = cls._get_or_create_default_schedule(rp_client, project_id, region)

        create_rp_kwargs = dict(
            project=project_id,
            disk=disk_name,
            body={
                "resourcePolicies": [default_rp],
            },
            requestId=gen_request_id(cls.__name__, resource),
        )

        if resource.resource_type == "compute.googleapis.com/Disk":
            create_rp_kwargs["zone"] = location
        elif resource.resource_type == "compute.googleapis.com/RegionDisk":
            create_rp_kwargs["region"] = region

        req = resource.service.addResourcePolicies(**create_rp_kwargs)
        req.execute()

    @classmethod
    def _get_rp_client(cls, **kwargs):
        rp = build_subresource("compute.resourcePolicies", "v1", **kwargs)
        return rp

    @classmethod
    def _is_snapshot_schedule(cls, rp_client, rp):
        project_id = get_url_value_by_collection(rp, "projects")
        region = get_url_value_by_collection(rp, "regions")
        name = get_url_value_by_collection(rp, "resourcePolicies")

        req = rp_client.get(
            project=project_id,
            region=region,
            resourcePolicy=name,
        )
        resp = req.execute()
        return "snapshotSchedulePolicy" in resp

    @classmethod
    def _get_or_create_default_schedule(cls, rp_client, project_id, region):
        rp_name = f"projects/{project_id}/regions/{region}/resourcePolicies/{cls.DEFAULT_RESOURCE_POLICY_NAME}"

        try:
            req = rp_client.get(
                project=project_id,
                region=region,
                resourcePolicy=cls.DEFAULT_RESOURCE_POLICY_NAME,
            )
            req.execute()
            return rp_name
        except googleapiclient.errors.HttpError as ex:
            if ex.resp.status != 404:
                raise

        # If it doesnt exist, let's create it
        body = {
            "name": cls.DEFAULT_RESOURCE_POLICY_NAME,
            "description": "The default backup schedule applied by "
            "ClearDATA safeguards. Changing this schedule "
            "will affect all disks associated with it.",
            "region": region,
            "snapshotSchedulePolicy": {
                "retentionPolicy": {
                    "maxRetentionDays": 30,
                    "onSourceDiskDelete": "APPLY_RETENTION_POLICY",
                },
                "schedule": {
                    "dailySchedule": {
                        "daysInCycle": 1,
                        "startTime": "00:30",
                    }
                },
            },
        }

        rp_client.insert(
            project=project_id,
            region=region,
            body=body,
        ).execute()

        return rp_name


class ComputeDisksRequireSnapshotSchedules(_DiskSnapshotBase):
    applies_to = ["compute.googleapis.com/Disk"]

    policy_attributes = {
        "description": _DiskSnapshotBase.description,
        "evaluation": "A compute disk is considered compliant if it has at least 1 snapshot schedule policy attached",
        "remediation": "A compute disk is remediated by attaching a snapshot schedule to the disk",
    }


class ComputeRegionDisksRequireSnapshotSchedules(_DiskSnapshotBase):
    applies_to = ["compute.googleapis.com/RegionDisk"]
    policy_attributes = {
        "description": "Require compute region disks to have snapshot schedules",
        "evaluation": "A compute region disk is considered compliant if it has at least 1 snapshot schedule policy attached",
        "remediation": "A compute region disk is remediated by attaching a snapshot schedule to the disk",
    }
