import jmespath
from googleapiclient.discovery import build
from collections import defaultdict


class CloudresourcemanagerProjectsRequireServiceAccountRoleSeparation:
    description = (
        "Restrict the use of an identity which both Service Account Admin and Service Account User "
        "role access to the project. Applies to only identities granted access to the the Project Level, "
        "inherited identities excluded from check."
    )
    applies_to = ["cloudresourcemanager.googleapis.com/Project"]
    policy_attributes = {
        "description": description,
        "evaluation": "A project is compliant if no identity in the project "
        "has both Service Account Admin and Service Account User access. Applies to only identities "
        "granted access to the the Project Level, inherited identities excluded from check.",
        "remediation": "N/A",
    }

    role_list = {
        "roles/iam.serviceAccountAdmin",
        "roles/iam.serviceAccountUser",
    }

    @classmethod
    def _get_project_iam_bindings(cls, resource):
        service = build("cloudresourcemanager", "v1", **resource.client_kwargs)
        policy = service.projects().getIamPolicy(resource=resource.project_id).execute()
        return policy.get("bindings")

    @classmethod
    def _over_privileged_identity(cls, bindings):
        role_bindings = defaultdict(set)
        for binding in bindings:
            role = binding["role"]
            if role in cls.role_list:
                for member in binding["members"]:
                    role_bindings[member].add(role)
                    if cls.role_list.issubset(role_bindings[member]):
                        return True
        return False

    @classmethod
    def compliant(cls, resource):
        bindings = cls._get_project_iam_bindings(resource)
        if cls._over_privileged_identity(bindings):
            return False
        return True

    @classmethod
    def excluded(cls, resource):
        safeguard_config = jmespath.search(
            'resource.labels."cleardata-safeguards"', resource.get()
        )
        return safeguard_config == "disabled"
