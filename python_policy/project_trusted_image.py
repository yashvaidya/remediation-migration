import jmespath
from googleapiclienthelpers.discovery import build_subresource


class CloudresourcemanagerProjectsRequireTrustedImagePolicy:
    description = "Projects can only use images allowed by OrgPolicy"
    applies_to = ["cloudresourcemanager.googleapis.com/Project"]

    policy_attributes = {
        "description": description,
        "evaluation": "A project is compliant if the image access "
        "policy contains only allowed values for compute.trustedImageProjects.",
        "remediation": "N/A",
    }

    @classmethod
    def compliant(cls, resource):
        service = build_subresource(
            "cloudresourcemanager.projects", "v1", **resource.client_kwargs
        )

        org_policy_request_body = {
            "constraint": "constraints/compute.trustedImageProjects"
        }

        request = service.getEffectiveOrgPolicy(
            resource=f"projects/{resource.project_id}", body=org_policy_request_body
        )
        policy = request.execute()

        if not policy.get("listPolicy"):
            return False

        if (
            policy["listPolicy"].get("allValues")
            and policy["listPolicy"]["allValues"] == "ALLOW"
        ):
            return False

        allowed_values = {
            "projects/cleardata-images",
            "projects/cos-cloud",
            "projects/dataflow-service-producer-prod",
            "projects/serverless-vpc-access-images",
            # gcp.compute.harden_images_project from config
            "projects/cleardata-comply-images",
            "projects/cleardata-internal-images",
        }

        if set(policy["listPolicy"]["allowedValues"]).difference(allowed_values):
            return False

        return True

    @classmethod
    def excluded(cls, resource):
        safeguard_config = jmespath.search(
            'resource.labels."cleardata-safeguards"', resource.get()
        )
        return safeguard_config == "disabled"
