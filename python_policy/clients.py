import os

from cleardata.api_tools import CleardataApiClient, CleardataAuth

app_name = "micromanager"

# This is a lousy hack to support generating docs. Since the python policy is
# loaded on-the-fly by rpelib, we can't properly mock things (or I havent
# figured out how). Setting this env var properly lets us load the policies
# without having auth setup
if os.environ.get("SKIP_CLIENTS", "").lower() == "true":
    CREDENTIALS_BASE_URL = None
    INVENTORY_BASE_URL = None

    def auth(r):
        return r

else:
    CREDENTIALS_BASE_URL = os.environ["CREDENTIALS_BASE_URL"]
    INVENTORY_BASE_URL = os.environ["INVENTORY_BASE_URL"]
    auth = CleardataAuth()

credentials = CleardataApiClient(
    CREDENTIALS_BASE_URL,
    auth=auth,
    app_name=app_name,
)

inventory = CleardataApiClient(
    INVENTORY_BASE_URL,
    auth=auth,
    app_name=app_name,
)
