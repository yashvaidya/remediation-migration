# Policies
from .compute_instances_patching import (  # noqa F401
    ComputeInstancesRequireUpToDatePatching,
)
from .compute_network_block_dhcp import (  # noqa f401
    ComputeNetworksProhibitInboundDHCPResponses,
)
from .dataproc_clusters_require_hardened_images import (  # noqa F401
    DataprocClustersRequireHardenedImages,
)
from .detect_vpc_service_controls import (  # noqa F401
    CloudresourcemanagerOrganizationsRequiresServiceControlsSetup,
)
from .event_log_sink import CloudresourcemanagerProjectsRequireEventsSink  # noqa F401
from .identities_list import (  # noqa F401
    CloudresourcemanagerOrganizationsDisallowProjectAccessToUnauthorizedEntities,
)
from .log_backup import CloudresourcemanagerProjectsRequireLogBackups  # noqa F401
from .project_lien import CloudresourcemanagerProjectsRequireClearDATALien  # noqa F401
from .project_trusted_image import (  # noqa F401
    CloudresourcemanagerProjectsRequireTrustedImagePolicy,
)
from .service_accounts_over_privileged_access import (  # noqa F401
    CloudresourcemanagerProjectsRestrictOverPrivilegedServiceAccount,
)
from .service_accounts_wide_scope import (  # noqa F401
    CloudresourcemanagerProjectsRestrictDefaultServiceAccountsGrants,
)
from .over_priveleged_service_account_user import (  # noqa F401
    CloudresourcemanagerProjectsRestrictOverPrivilegedServiceAccountUser,
)
from .disk_snapshots import (  # noqa F401
    ComputeDisksRequireSnapshotSchedules,
    ComputeRegionDisksRequireSnapshotSchedules,
)
from .service_account_role_separation import (  # noqa F401
    CloudresourcemanagerProjectsRequireServiceAccountRoleSeparation,
)
from .over_privileged_identity import CloudresourcemanagerProjectsRestrictOverPrivilegedIdentity