import os
import random
import re
import string
import uuid
from datetime import datetime, timezone

import jmespath
import requests
import tenacity
from googleapiclient.errors import HttpError

from . import clients

config = {}


# Some requests support passing a requestId so duplicate requests don't fail
# this generates a uuid from the input so its the same if we try to remediate
# the same thing multiple times
def gen_request_id(policy_name, resource):
    resource_name = resource.full_resource_name()
    return str(uuid.uuid5(uuid.NAMESPACE_DNS, f"{resource_name}/{policy_name}"))


@tenacity.retry(
    wait=tenacity.wait_random_exponential(multiplier=15, max=60),
    stop=tenacity.stop_after_attempt(3),
)
def get_config_context(lookup_path):
    global config
    if not config:
        resp = requests.get("http://localhost:8181/v1/data").json()

        try:
            config = resp["result"]["config"]
        except Exception as e:
            print("Failed to retrieve config")
            raise e

    return jmespath.search(lookup_path, config)


def get_bucket_iam(buckets_client, bucket_name):
    return buckets_client.getIamPolicy(bucket=bucket_name).execute()


def get_logging_sink(sinks_client, project_id, sink_name):
    sink_id = f"projects/{project_id}/sinks/{sink_name}"

    try:
        req = sinks_client.get(sinkName=sink_id)
        return req.execute()
    except HttpError as ex:
        if ex.resp.status == 404:
            return None
        raise ex


def update_logging_sink(sinks_client, project_id, sink_name, sink_data):
    req = sinks_client.update(
        sinkName=f"projects/{project_id}/sinks/{sink_name}",
        uniqueWriterIdentity=True,
        body=sink_data,
    )

    return req.execute()


def create_logging_sink(sinks_client, project_id, sink_name, sink_data):
    req = sinks_client.create(
        parent=f"projects/{project_id}", uniqueWriterIdentity=True, body=sink_data
    )

    return req.execute()


def compare_logging_sinks(expected, actual):
    for k, v in expected.items():
        if actual.get(k, "") != v:
            return False

    return True


def get_project(project_id):
    project_data = clients.credentials.get(f"/projects/{project_id}").json()

    return project_data


def fromiso_tz(dt_str):
    # Assume its UTC if we dont know cause python is the worst
    dt = datetime.fromisoformat(dt_str)
    if dt.tzinfo is None:
        return dt.replace(tzinfo=timezone.utc)
    return dt


def get_url_value_by_collection(url, collection):
    m = re.search(rf"\/{collection}\/([^\/]+)(\/|$)", url)
    if m:
        return m.group(1)
    return None


def get_env():
    env = os.environ.get("ENV")
    if env in ["prod", "nonprod"]:
        return env

    return "local"

def get_credentials_project():
    credentials_project = (
        "cleardata-credentials"
        if (get_env() == "prod")
        else "cleardata-creds-nonprod"
    )
    return credentials_project

def get_user_managed_service_account_regex():
    user_managed_service_account_regex = re.compile(
        r"serviceAccount:(?!service-)[a-zA-Z0-9_.-]+@([a-zA-Z0-9_.-]+).iam.gserviceaccount.com"
    )
    return user_managed_service_account_regex


def get_cleardata_service_account_regex():
    credentials_project = get_credentials_project()
    cleardata_service_account_regex = re.compile(
        rf"serviceAccount:(cdsa-)[a-zA-Z0-9_.-]+@({credentials_project}).iam.gserviceaccount.com"
    )
    return cleardata_service_account_regex


def randstr(strlen=5):
    return "".join([random.choice(string.ascii_lowercase) for i in range(strlen)])
