import re

from googleapiclient.discovery import build

PROJECTS_PAGE_SIZE = 50
FOLDERS_PAGE_SIZE = 50

# hardcode cleardata project ids, we should not have too many and we
# don't want to issue a separate query for each identity to check if it's project
# belongs to cleardata
CLEARDATA_PROJECT_IDS = ["cleardata-credentials"]


class IdentityEvaluator:
    user_managed_service_account_regex = re.compile(
        r"serviceAccount:(?!service-)[a-zA-Z0-9_.-]+@([a-zA-Z0-9_.-]+).iam.gserviceaccount.com"
    )
    google_app_engine_regex = re.compile(
        r"serviceAccount:([a-zA-Z0-9_.-]+)@appspot.gserviceaccount.com"
    )

    google_service_account_regex = re.compile(
        r"serviceAccount:service-(\d+)@[a-zA-Z0-9_.-]+.iam.gserviceaccount.com"
    )
    google_project_account_regex = re.compile(
        r"serviceAccount:(\d+)@cloudservices.gserviceaccount.com"
    )
    google_compute_engine_regex = re.compile(
        r"serviceAccount:(\d+)-compute@developer.gserviceaccount.com"
    )

    regexes_by_project_id = [
        user_managed_service_account_regex,
        google_app_engine_regex,
    ]
    regexes_by_project_number = [
        google_service_account_regex,
        google_project_account_regex,
        google_compute_engine_regex,
    ]

    def __init__(self, organization, project_ids, project_numbers, identities):
        self.organization = organization
        self.project_ids = project_ids
        self.project_numbers = project_numbers
        self.identities = identities

    def evaluate(self):
        not_valid_identities = set()

        for identity in self.identities:
            if identity.startswith("user:"):
                if identity.split("@")[-1] not in [
                    self.organization["displayName"],
                    "cleardata.com",
                ]:
                    not_valid_identities.add(identity)

            if identity.startswith("serviceAccount:"):
                self._evaluate_by_project_id(identity, not_valid_identities)
                self._evaluate_by_project_number(identity, not_valid_identities)

        return not_valid_identities

    def _evaluate_by_project_id(self, identity, not_valid_identities):
        for regex in self.regexes_by_project_id:
            found = regex.findall(identity)

            if found and found[0] not in self.project_ids:
                not_valid_identities.add(identity)
                return

    def _evaluate_by_project_number(self, identity, not_valid_identities):
        for regex in self.regexes_by_project_number:
            found = regex.findall(identity)

            if found and found[0] not in self.project_numbers:
                not_valid_identities.add(identity)
                return


class CloudresourcemanagerOrganizationsDisallowProjectAccessToUnauthorizedEntities:
    description = "Identities which has access to Projects must belong to Organization, ClearData or Google"
    applies_to = ["cloudresourcemanager.googleapis.com/Organization"]

    @classmethod
    def compliant(cls, resource):
        projects_service_v1 = build(
            "cloudresourcemanager", "v1", **resource.client_kwargs
        )
        projects_service_v2 = build(
            "cloudresourcemanager", "v2", **resource.client_kwargs
        )

        org_id = resource.to_dict()["name"]
        org = (
            projects_service_v1.organizations()
            .get(name=f"organizations/{org_id}")
            .execute()
        )

        # Get all folder ID's from organization level and all sub-folders.
        all_folder_ids = []
        cls._get_folders_from_parent(
            projects_service_v2, "organizations", org_id, all_folder_ids
        )

        # Get all projects from the organization level.
        all_projects = []
        cls._get_projects_from_parent(
            projects_service_v1, "organization", org_id, all_projects
        )

        # Get all projects from each folder.
        for folder_id in all_folder_ids:
            cls._get_projects_from_parent(
                projects_service_v1, "folder", folder_id, all_projects
            )

        project_ids = {project["projectId"] for project in all_projects}.union(
            CLEARDATA_PROJECT_IDS
        )
        project_numbers = {project["projectNumber"] for project in all_projects}
        iam_policies = cls._get_all_project_iam_policies(resource)
        identities = cls._get_all_identities_from_iam_policies(iam_policies)

        evaluator = IdentityEvaluator(
            organization=org,
            project_ids=project_ids,
            project_numbers=project_numbers,
            identities=identities,
        )
        not_valid_identities = evaluator.evaluate()

        return False if not_valid_identities else True

    @classmethod
    def excluded(cls, resource):
        return False

    @classmethod
    def _get_projects_from_parent(
        cls, service, parent_type, parent_id, all_projects, page_token=None
    ):
        project_filter = f"parent.type:{parent_type} parent.id: {parent_id}"

        response_data = (
            service.projects()
            .list(
                filter=project_filter, pageSize=PROJECTS_PAGE_SIZE, pageToken=page_token
            )
            .execute()
        )

        if "projects" in response_data:
            all_projects.extend(response_data["projects"])

        next_page_token = response_data.get("nextPageToken")
        if next_page_token:
            cls._get_projects_from_parent(
                service, parent_type, parent_id, all_projects, next_page_token
            )

    @classmethod
    def _get_folders_from_parent(
        cls, service, parent_type, parent_id, folder_ids, page_token=None
    ):
        response_data = (
            service.folders()
            .list(
                parent=f"{parent_type}/{parent_id}",
                pageSize=FOLDERS_PAGE_SIZE,
                pageToken=page_token,
            )
            .execute()
        )

        # Paginate on the current_level.
        next_page_token = response_data.get("nextPageToken")
        if next_page_token:
            cls._get_folders_from_parent(
                service, parent_type, parent_id, folder_ids, next_page_token
            )

        for folder in response_data.get("folders", []):
            folder_id = folder["name"].split("/")[
                -1
            ]  # Its format is `folders/{folder_id}`, example: "folders/1234".
            folder_ids.append(folder_id)

            # Pass each folder of the parent and jump on the next level.
            cls._get_folders_from_parent(service, "folders", folder_id, folder_ids)

    @classmethod
    def _get_all_project_iam_policies(cls, resource):
        assets_service = build("cloudasset", "v1", **resource.client_kwargs)
        policies = []
        response = (
            assets_service.v1()
            .searchAllIamPolicies(
                scope=f"organizations/{resource.to_dict()['name']}",
                query='resource : ("cloudresourcemanager")',
            )
            .execute()
        )
        policies.extend(response["results"])
        page_token = response.get("nextPageToken")

        while page_token is not None:
            response = (
                assets_service.v1()
                .searchAllIamPolicies(
                    scope=f"organizations/{resource.to_dict()['name']}",
                    query='resource : ("cloudresourcemanager" AND "projects")',
                    pageToken=page_token,
                )
                .execute()
            )

            policies.extend(response["results"])

            page_token = response.get("nextPageToken")
        return policies

    @classmethod
    def _get_all_identities_from_iam_policies(cls, policies):
        identities = []
        for policy in policies:
            for binding in policy["policy"]["bindings"]:
                identities.extend(binding["members"])
        return identities

    @classmethod
    def _collect_identities_and_projects_info(cls, resource, projects, org_id):
        assets_service = build("cloudasset", "v1p4beta1", **resource.client_kwargs)

        project_ids = set()
        project_numbers = set()
        identities = set()

        if projects:
            for project in projects:
                project_ids.add(project["projectId"])
                project_numbers.add(project["projectNumber"])

                query = f'//cloudresourcemanager.googleapis.com/projects/{project["projectId"]}'
                results = (
                    assets_service.v1p4beta1()
                    .analyzeIamPolicy(
                        parent=f"organizations/{org_id}",
                        analysisQuery_resourceSelector_fullResourceName=query,
                    )
                    .execute()
                )

                for result in results["mainAnalysis"]["analysisResults"]:
                    for identity in result["identityList"]["identities"]:
                        identities.add(identity["name"])

        return project_ids, project_numbers, identities
