from cleardata.api_tools import CleardataApiClient,  GcpProjectCredentials
from googleapiclienthelpers.discovery import build_subresource
import os

api_client = CleardataApiClient(
    os.environ["CREDENTIALS_BASE_URL"],
    app_name="trend_removal",
)
for project in api_client.resultinate("/projects"):
    project_id = project["project_id"]
    creds = GcpProjectCredentials(api_client, project_id, 'owner')
    project_client = build_subresource("compute.projects", "v1", credentials=creds)
    req = project_client.get(project=project_id)
    response = req.execute()

    instance_metadata = response["commonInstanceMetadata"]
    metadata_items = []
    try:
        metadata_items = instance_metadata["items"]
    except KeyError:
        continue

    updated_metadata_list = []

    for item in metadata_items:
        if item['key'] == 'cleardata-trend-micro-dsm-tenantid' or item['key'] == 'cleardata-trend-micro-dsm-token':
            continue
        else:
            updated_metadata_list.append({"key": item['key'], "value": item['value']})

    instance_metadata["items"] = updated_metadata_list
    req = project_client.setCommonInstanceMetadata(
                project=project_id, body=instance_metadata
            )
    req.execute()
