from cleardata.api_tools import CleardataApiClient, CleardataGraphClient
import os

graph_client = CleardataGraphClient()
as_config_client = CleardataApiClient(
    os.environ["AS_CONFIG_BASE_URL"],
    app_name='remediation-migration-script',
)
api_client = CleardataApiClient(
                os.environ["CREDENTIALS_BASE_URL"],
                app_name="remediation-migration-script",
            )

mutation_query = """
mutation ($cloudAccount: CloudConfigurationAccountInput!){
  updateCloudAccount(cloudAccount: $cloudAccount) {
    isSuccessful
    errorMessages
    response {
      customerId
      cloudAccountId
      platform
      cloudAccountName
      status
      orderId
    }
  }
}
"""

def update_graph(project_id, customer_id, remediation_state):
    variables = {
        "cloudAccount":
            {
                "cloudAccountId": project_id,
                "customerId": customer_id,
                "platform": "gcp",
                "remediationEnabled": remediation_state
            }
    }
    graph_client.post_to_graph(query=mutation_query, variables=variables)
    

for project in api_client.resultinate("/projects"):
    project_id = project["project_id"]
    customer_id = project["customer_id"]
    resp = as_config_client.get(
        f'/projects/{project_id}/remediation')
    remediation_state = resp.json()['project_remediation']
    if remediation_state is False:
      update_graph(project_id, customer_id, remediation_state)
